#!/bin/bash -e

set -e

echo "-- version of wget -------------------------"
wget --version
echo "-- version of curl -------------------------"
curl --version

echo "-- version of perl -------------------------"
perl --version

echo "-- version of python3 ----------------------"
python3 --version
echo "-- version of pip3 -------------------------"
pip3 --version

echo "-- version of git --------------------------"
git --version

echo "-- version of make -------------------------"
make --version
echo "-- version of cmake ------------------------"
cmake --version

echo "-- version of valgrind ---------------------"
valgrind --version

echo "-- version of ccache -----------------------"
ccache --version
